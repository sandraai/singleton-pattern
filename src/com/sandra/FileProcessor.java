package com.sandra;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileProcessor {

    public void printContents (String fileName) {

        Path filePath = Paths.get(ConfigurationSettingsSingleton.INSTANCE.getFilePath(), fileName);

        try (Stream<String> lines = Files.lines(filePath)) {
            lines.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
